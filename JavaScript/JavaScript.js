$( document ).ready(function() {
	var navOffset = $('.header-menu').offset().top;
	$(window).scroll(function(){
	    var scrolled = $(this).scrollTop();
	    if (scrolled > navOffset) {
	    	$('.header').addClass('nav__fixed');
	    }
	    else if(scrolled < navOffset) {
	    	$('.header').removeClass('nav__fixed');
	    }
	});

	$('.hamburger').on('click', function(){
      $('.burger-menu').slideToggle(600);
      $('.hamburger__line').toggleClass('is-active');
   });
	$('.header-menu__search').on('click', function(){
      $('.search-block').slideToggle(600);
   });

	$(".overview__more").on('click', function() {
   	$(this).parent(".overview__footer").find('.overview__hide').slideToggle();
  	});
  	$(".aside-card__link").on('click', function() {
      $(this).parent(".aside-card__excert ").toggleClass("show-content").find('.aside-card__hide').slideToggle();

      var replaceText = $(this).parent().hasClass("show-content") ? "Less More" : "Read More";
      $(this).text(replaceText);
   });
   $(".block-card__link").on('click', function() {
      $(this).parent(".block-card__information").toggleClass("show-content").find('.block-card__hide').slideToggle();

      var replaceText = $(this).parent().hasClass("show-content") ? "Less More" : "Read More";
      $(this).text(replaceText);
   });
});